#!/bin/bash

fold -2 | awk '
BEGIN{printf "{ "}
{
if(NR!=1){
    printf ","
}
printf "0x" $1
}
END{printf " }"}
'
